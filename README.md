# Projekt Übersicht Cloud TRANSIT

## Betriebene Systeme

- [OpenStack Cluster (Login)](http://cloudtransit-os.fh-luebeck.de/horizon)
- [Snippits und Anleitungen für OpenStack und dem BladeCenter](https://bitbucket.org/pcq/bladecenter_manuals)
## Automatisierungs Skript

- [OpenStack Juno Multinode installation](https://bitbucket.org/pcq/openstack_juno_multinode_installation)
- [Blade Center Security Scripts](https://bitbucket.org/pcq/bladecenter-security-scripts)
- [Openstack und BladeCenter-Verwaltungstools](https://bitbucket.org/cfstue/blade-and-openstack-management) Floating-IP Generierung, Glace und Nova Restart-Script, Netzwerktools für die Blades
- [Kubernetes Installation](https://bitbucket.org/cfstue/kube_install)
## Tools

- [EasyCompare](https://bitbucket.org/nanekratzke/easycompare) Cloud Instance Comparison Tool
- [PingPong](https://github.com/nkratzke/pingpong) Benchmarking Tool (Container, SDN and REST impact)
## Paper

### 2016
- EDOC 2016: [ClouNS - A Reference Model for Cloud-Native Applications](https://bitbucket.org/nanekratzke/2016-edoc)
- Closer 2016: [ppbench - A Visualizing Network Benchmark for Microservices](https://bitbucket.org/nanekratzke/2016-closer)
- Cloud Computing 2016: [Overcome Vendor Lock-In by Integrating Already Available Container Technologies](https://bitbucket.org/pcq/2016-iaria/overview)
- OBJEKTspektrum Online Themenspecials 2016: [Vendor Lock-In im Cloud Computing! Was bringen Container und Container-Cluster?](https://bitbucket.org/pcq/2016-objspektrum)
### 2015
- NetSer Journal: [How to Operate Container Clusters more Efficiently? Some Insights Concerning Containers, Software-Defined-Networks, and their sometimes Counterintuitive Impact on Network Performance](https://bitbucket.org/nanekratzke/2015-netser/)
- JCCR Journal: [About Automatic Benchmarking of IaaS Cloud Service Providers for a World of Container Clusters](https://bitbucket.org/nanekratzke/2015-jccr)
- Cloud Computing 2015: [About Microservices, Containers and their Underestimated Impact on Network Performance](https://bitbucket.org/nanekratzke/2015-cloud-computing)

## Sonstiges
- [Werbematerial](https://bitbucket.org/pcq/2016-cloudtransit_advt) Logo, Poster und Lesezeichen
## Automatisierungs Skript
- [Network Information Service und Network File System](https://bitbucket.org/cfstue/nis_nfs_script)
- [Kerberos Identifikationsservice](https://bitbucket.org/cfstue/krb_script) für Server und Clients